package fr.jctarby.twoactivities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import fr.jctarby.twoactivities.ui.theme.TwoActivitiesTheme

// Cette première activité récupère le contenu du Text et l'envoie à la seconde activité.
// Notez l'usage de lambda fonction pour garder le côté générique des Composables et ne
// pas avoir à gérer des données complexes comme le contexte par exemple.
class MainActivity : ComponentActivity() {

    //la chaine affichée au départ et envoyée à la seconde activité. Notez l'usage de State, donc
    // mise à jour automatique de l'IHM si besoin.
    private var currentText by mutableStateOf("Hello from First Activity")


    // comportement pour l'appel de la seconde activité et la gestion du retour.
    private val startSecondActivityForResult = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val returnedText = result.data?.getStringExtra("returnedText")
            handleResult(returnedText)
        } else if (result.resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Back cliqué", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TwoActivitiesTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    // Écran 1.
                    // On utilise :
                    // - en premier paramètre : une lamba fonction pour fournir le coomportement à
                    //      adopter par les éléments de l'IHM (ici le bouton)
                    // - en second paramètre : le texte à faire affiche dans l'IHM
                    Screen1(
                            {
                                // Lance l'écran 2 avec le paramètre (le texte)
                                val intent = Intent(this@MainActivity, SecondActivity::class.java)
                                intent.putExtra("paramFromActivity1", currentText)
                                startSecondActivityForResult.launch(intent)
                            },
                            text = currentText)
                }
            }
        }
    }

    private fun handleResult(result: String?) {
        if (!result.isNullOrBlank()) {
            currentText = result // MAJ auto de l'IHM puisque currentText est un State
        } else {
            Toast.makeText(this, "Chaine vide", Toast.LENGTH_SHORT).show()
        }
    }

}

// le Composable avec le comportement à adopter par le bouton
@Composable
fun Screen1(onClick: () -> Unit, text: String) {
    Column {
        Text(text = text,
                style = TextStyle(fontSize = 50.sp))
        Button(onClick = onClick) {
            Text(text = "Pass to Second Activity")
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    TwoActivitiesTheme {
        Screen1(onClick = { }, text = "preview")
    }
}