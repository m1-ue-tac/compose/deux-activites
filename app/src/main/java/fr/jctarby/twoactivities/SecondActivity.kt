package fr.jctarby.twoactivities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import fr.jctarby.twoactivities.ui.theme.TwoActivitiesTheme

// La seconde activité reçoit le paramètre de la première activité et l'affiche dans une zone de
// saisie. Le bouton permet de renvoyer cette zone de saisie (modifiable) à la première activité.
// Si on fait un BACK, l'activité 1 le saura et ne mettra pas à jour son texte.
class SecondActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    //  val selectedItem = intent.getStringExtra("paramFromActivity1") ?: ""
        val selectedItem = intent.getStringExtra("paramFromActivity1").orEmpty()

        setContent {
            TwoActivitiesTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    // Écran 2 avec :
                    // - le comportement à adopter par le bouton. 'enteredText' est le texte envoyé
                    //   lors du clic sur le bouton.
                    // - le paramètre reçu lors du clic sur le bouton, et à faire afficher
                    Screen2(
                            { texteSaisi ->
                                val modifiedText = if (texteSaisi == "") "" else
                                    "Modified " + texteSaisi
                                val resultIntent = Intent().apply {
                                    putExtra("returnedText", modifiedText)
                                }
                                setResult(Activity.RESULT_OK, resultIntent)
                                finish()
                            }, text = selectedItem)
                }
            }
        }
    }


    // La solution du onBackPressed n'est pas la meilleure (dépréciée). Nous verrons
    // plus tard comment gérer cela avec le composant Navigation
    override fun onBackPressed() {
        // si on veut renvoyer malgré tout des donnée...
        //        val data = intent.getStringExtra("paramFromActivity1").orEmpty()
        //        val resultIntent = Intent().apply {
        //            putExtra("returnedText", data)
        //        }
        //        setResult(Activity.RESULT_CANCELED, resultIntent)
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}


// L'écran avec :
// - le comportement du bouton
// - le champ texte initialisé avec la donnée envoyée par l'activité
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Screen2(onClick: (String?) -> Unit, text: String) {

    // variable pour gérer le contenu du champ de saisie
    var enteredText by remember { mutableStateOf(text) }

    Column {
        TextField(value = enteredText, onValueChange = { enteredText = it })
        Button(onClick = { onClick(enteredText) }) {
            Text(text = "Return to first activity")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview2() {
    TwoActivitiesTheme {
        Screen2(onClick = {}, text = "preview")
    }
}
